import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { Link, hashHistory } from 'react-router';
import fetchSongs from '../queries/fetchSongs';
class SongCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
  }

  onChangeHandler(e) {
    this.setState({
      title: e.target.value,
    });
  }
  onSubmitHandler(e) {
    e.preventDefault();
    this.props.mutate({
      variables: {
    title: this.state.title
      },
      refetchQueries: [{
        query: fetchSongs
      }]
    }).then(() => hashHistory.push('/'))
  }
  render() {
    return (
      <div>
<Link to="/">Back</Link>
        <h3>Create a new song</h3>
        <form onSubmit={(e)=> this.onSubmitHandler(e)}>
          <label>Song Title</label>
          <input
            type='text'
            onChange={(e) => this.onChangeHandler(e)}
            value={this.state.title}
          />
        </form>
      </div>
    );
  }
}

const mutation = gql`
mutation AddSong($title: String){
  addSong(title: $title) {
    title
  }
}
`
export default graphql(mutation)(SongCreate);
