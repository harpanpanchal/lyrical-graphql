import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

class LyricCreate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      content: ''
    }
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
  }
  onChangeHandler(e) {

    this.setState({
      content: e.target.value
    })
  }
  onSubmitHandler(e) {
    e.preventDefault();
    this.props.mutate({
      variables: {
        content: this.state.content,
        songId: this.props.songId
  }
    }).then(() => this.setState({
      content: ''
    }))
    

  
  }
  render() {
    return (<div><form onSubmit={(e)=>this.onSubmitHandler(e)}>
      <label>Add a Lyric</label>
      <input type="text" value={this.state.content} onChange={(e)=>this.onChangeHandler(e) } />
    </form></div>)
  }
}

const mutation = gql`
mutation AddLyricToSong($songId: ID!, $content: String){
  addLyricToSong(songId: $songId, content: $content){
    id
    lyrics{
      id
      content
      likes
    }
  }
}
`

export default graphql(mutation)(LyricCreate);