Lyrical-GraphQL
Framework and tools:

React.js
Mongo DB (Online services)
Node - Express (Server)
GraphQL
Please refer following command to run the project.

Open terminal or built in terminal in the project directory and type in following commands.
A. 'npm install' 
B. 'npm run dev'

Few settings are required to set up in MongoDB (online) by signing up free account and free hosting services for MongoDB and also use the db connection string along with the db credentials from the MongoDB online account. Please refer https://account.mongodb.com/ for further reference. Server.js file needs to be updated with the correct DB string along with the password.

Codebase
All the front end (React.js) codebase can be found within 'Client' folder. Refer 'components' for React.js and 'queries' folder for graphQL queries. ApolloClient has been used for front-end with React and Express-Graphql for back-end.